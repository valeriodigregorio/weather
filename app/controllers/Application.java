package controllers;

import play.Logger;
import play.mvc.Controller;
import play.mvc.Http;

/**
 * Main application controller.
 * 
 * @author Valerio Di Gregorio
 */
public class Application extends Controller {

	/**
	 * Render the homepage or the personal page of the user if an existing
	 * session is still alive.
	 */
	public static void index() {
		Logger.debug("[Application.index]");
		if (Http.Request.current().cookies.get("WEATHER_SESSION") != null) {
			// redirect the users to their personal page if a session
			// already exists and it is still alive
			Weather.index();
		}
		render();
	}

}