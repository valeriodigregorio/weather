package controllers;

import com.netflix.astyanax.connectionpool.exceptions.ConnectionException;

import play.Logger;
import play.mvc.Http;
import models.Data;
import models.User;

public class Security extends Secure.Security {

	static boolean authenticate(String username, String password) {
		Logger.debug("[Security.authenticate] username = %s; password = %s", username, password);
		return User.validate(username, password);
	}

	static void onDisconnected() {
		Logger.debug("[Security.onDisconnected]");
		String ipaddress = Http.Request.current().remoteAddress;
		try {
			Data.tombstone(ipaddress);
		} catch (ConnectionException e) {
			Logger.warn(e.getMessage());
		}
		Application.index();
	}

	static void onAuthenticated() {
		Logger.debug("[Security.onAuthenticated]");
		Weather.index();
	}

}
