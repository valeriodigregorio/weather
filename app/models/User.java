package models;

import java.util.Iterator;

import play.Logger;

import com.netflix.astyanax.ColumnListMutation;
import com.netflix.astyanax.Keyspace;
import com.netflix.astyanax.MutationBatch;
import com.netflix.astyanax.connectionpool.OperationResult;
import com.netflix.astyanax.connectionpool.exceptions.ConnectionException;
import com.netflix.astyanax.model.ColumnFamily;
import com.netflix.astyanax.model.ColumnList;
import com.netflix.astyanax.model.CqlResult;
import com.netflix.astyanax.model.Row;
import com.netflix.astyanax.model.Rows;
import com.netflix.astyanax.query.ColumnFamilyQuery;
import com.netflix.astyanax.query.CqlQuery;
import com.netflix.astyanax.serializers.StringSerializer;

/**
 * The user object.
 * 
 * @author Valerio Di Gregorio
 */
public class User {
	private static final ColumnFamily CF_USERS = new ColumnFamily<String, String>("users", StringSerializer.get(), StringSerializer.get());

	/**
	 * E-mail address.
	 */
	public String email;

	/**
	 * Password.
	 */
	public String password;

	/**
	 * Full name.
	 */
	public String fullname;

	/**
	 * Create a user loading the information from the DB.
	 * 
	 * @param email
	 *            The e-mail address.
	 *            
	 * @throws ConnectionException
	 *            Error connecting to the DB.
	 */
	public User(String email) throws ConnectionException {
		Logger.debug("[User.User] email = %s", email);
		String statement = String.format("SELECT * FROM %s WHERE %s = '%s';",
				CF_USERS.getName(), "email", email);
		
		OperationResult<CqlResult<Integer, String>> result = Database
				.getKeyspace()
				.prepareQuery(CF_USERS)
				.withCql(statement)
				.execute();
		
		Rows<Integer, String> rows = result.getResult().getRows();
		Iterator<Row<Integer, String>> it = rows.iterator();
		while(it.hasNext()) {
			ColumnList<String> cols = it.next().getColumns();
			try {
				this.password = cols.getColumnByName("password").getStringValue();
				this.fullname = cols.getColumnByName("fullname").getStringValue();
				this.email = cols.getColumnByName("email").getStringValue();
				return;
			} catch (NullPointerException e) {
				this.password = null;
				this.fullname = null;
				this.email = null;
				Logger.warn("[User.User] Invalid entry.");
			}
		}
	}
	
	/**
	 * Create a user.
	 * 
	 * @param email
	 *            The e-mail address.
	 * @param password
	 *            The password.
	 * @param fullname
	 *            The user's full name.
	 */
	public User(String email, String password, String fullname) {
		Logger.debug("[User.User] email = %s, password = %s, fullname = %s", email, password, fullname);
		this.email = email;
		this.password = password;
		this.fullname = fullname;
	}

	/**
	 * Validate the user e-mail address and password.
	 * 
	 * @param email
	 *            The e-mail address.
	 * @param password
	 *            The password.
	 * @param fullname
	 *            The user's full name.
	 * 
	 * @return Returns false if the user does not exist or its password is not
	 *         valid. Otherwise true.
	 */
	public static boolean validate(String email, String password) {
		Logger.debug("[User.validate] email = %s, password = %s", email, password);
		try {
			// I should use MD5 for the password and store the digest in the DB...
			User user = new User(email);
			return (user.password != null && user.password.compareTo(password) == 0);
		} catch (ConnectionException e) {
			Logger.error("[User.validate] Error retrieving user data.");
			return false;
		}
	}
	
}