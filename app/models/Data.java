package models;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import play.Logger;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.netflix.astyanax.connectionpool.OperationResult;
import com.netflix.astyanax.connectionpool.exceptions.ConnectionException;
import com.netflix.astyanax.model.ColumnFamily;
import com.netflix.astyanax.model.ColumnList;
import com.netflix.astyanax.model.CqlResult;
import com.netflix.astyanax.model.Row;
import com.netflix.astyanax.model.Rows;
import com.netflix.astyanax.serializers.StringSerializer;

/**
 * The weather data object.
 * 
 * @author Valerio Di Gregorio
 */
public class Data {
	private static final ColumnFamily CF_DATA = new ColumnFamily<String, String>("data", StringSerializer.get(), StringSerializer.get());

	/**
	 * The IP address.
	 */
	public String ipaddress;

	/**
	 * The weather data in JSON format.
	 */
	public String weather;
	
	/**
	 * Retrieve data for a specific session.
	 * 
	 * @param ipaddress
	 *            The IP address of the session.
	 * @return Return the data cached for the specified session or null in case
	 *         of error or no data available.
	 * 
	 * @throws ConnectionException
	 *             Error establishing a connection with the DB.
	 */
	public static Data get(String ipaddress) throws ConnectionException {
		Logger.debug("[Data.get] ipaddress = %s", ipaddress);
		
		String statement = String.format("SELECT * FROM %s WHERE %s = '%s';",
				CF_DATA.getName(), "ipaddress", ipaddress);
		Logger.debug("[Data.get] query = %s", statement);
		
		OperationResult<CqlResult<Integer, String>> result = Database
				.getKeyspace()
				.prepareQuery(CF_DATA)
				.withCql(statement)
				.execute();
				
		Rows<Integer, String> rows = result.getResult().getRows();
		Iterator<Row<Integer, String>> it = rows.iterator();
		while(it.hasNext()) {
			ColumnList<String> cols = it.next().getColumns();
			try {
				String json = cols.getColumnByName("weather").getStringValue();
				String ip = cols.getColumnByName("ipaddress").getStringValue();
				return new Data(ip, json);
			} catch (NullPointerException e) {
				Logger.warn("[Data.get] Invalid entry.");
			}
		}
		
		Logger.warn("[Data.get] No valid entries found.");
		return null;
	}
	
	/**
	 * Remove the data of a specific session.
	 * 
	 * @param ipaddress
	 *            The IP address of the session.
	 * 
	 * @throws ConnectionException
	 *             Error establishing a connection with the DB.
	 */
	public static void tombstone(String ipaddress) throws ConnectionException {
		Logger.debug("[Data.tombstone] ipaddress = %s", ipaddress);
		
		String statement = String.format("DELETE FROM %s WHERE %s = '%s';",
				CF_DATA.getName(), "ipaddress", ipaddress);
		Logger.debug("[Data.tombstone] query = %s", statement);
		
		Database.getKeyspace()
				.prepareQuery(CF_DATA)
				.withCql(statement)
				.execute();
	}
	
	/**
	 * Create a data object for the specified session.
	 * 
	 * @param ipaddress
	 *            The IP address of the session.
	 * @param weather
	 *            A string containing the date associated with the session.
	 */
	public Data(String ipaddress, String weather) {
		Logger.debug("[Data.Data] ipaddress = %s, json = %s", ipaddress, weather);
		this.ipaddress = ipaddress;
		this.weather = weather;
	}
	
	/**
	 * Store the current data into the DB.
	 * 
	 * @param ttl
	 *            Time to live of the stored data.
	 * 
	 * @throws ConnectionException
	 *             Error establishing a connection with the DB.
	 */
	public void store(long ttl) throws ConnectionException {
		Logger.debug("[Data.store]");
		
		String statement = String.format("INSERT INTO %s (%s, %s) VALUES ('%s', '%s') USING TTL %d;",
				CF_DATA.getName(), "ipaddress", "weather", ipaddress, weather, ttl);
		Logger.debug("[Data.store] query = %s", statement);
		
		Database.getKeyspace()
				.prepareQuery(CF_DATA)
				.withCql(statement)
				.execute();
	}
}