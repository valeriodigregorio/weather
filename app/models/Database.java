package models;
import play.Logger;

import com.google.common.collect.ImmutableMap;
import com.netflix.astyanax.AstyanaxConfiguration;
import com.netflix.astyanax.AstyanaxContext;
import com.netflix.astyanax.AstyanaxContext.Builder;
import com.netflix.astyanax.Keyspace;
import com.netflix.astyanax.connectionpool.NodeDiscoveryType;
import com.netflix.astyanax.connectionpool.exceptions.ConnectionException;
import com.netflix.astyanax.connectionpool.impl.ConnectionPoolConfigurationImpl;
import com.netflix.astyanax.connectionpool.impl.CountingConnectionPoolMonitor;
import com.netflix.astyanax.impl.AstyanaxConfigurationImpl;
import com.netflix.astyanax.thrift.ThriftFamilyFactory;

/**
 * The database.
 * 
 * @author Valerio Di Gregorio
 */
public class Database {
	
	/**
	 * Cluster name.
	 */
	private static final String CLUSTER_NAME = "Valerio-PC";
	
	/**
	 * Name of the connection pool.
	 */
	private static final String CONNECTIONPOOL_NAME = "MyConnectionPool";
	
	/**
	 * Host name of the database.
	 */
	private static final String DATABASE_HOST = "localhost";
	
	/**
	 * DB listening port.
	 */
	private static final int DATABASE_POST = 9160;
	
	/**
	 * Keyspace name.
	 */
	private static final String KEYSPACE_NAME = "weather";
	
	/**
	 * Instance of the DB.
	 */
	private static Database INSTANCE = null;

	/**
	 * Instance of the keyspace.
	 */
	private static Keyspace KEYSPACE = null;
	
	/**
	 * Retrieve the keyspace in use.
	 * 
	 * @return The keyspace in use.
	 */
	public static Keyspace getKeyspace() {
		Logger.debug("[Database.getKeyspace]");
		if(INSTANCE == null) {
			try {
				INSTANCE = new Database();
			} catch (ConnectionException e) {
				INSTANCE = null;
				KEYSPACE = null;
			}
		}
		return KEYSPACE;
	}
	
	/**
	 * Establish and initialize the connection with the DB.
	 * 
	 * @throws ConnectionException
	 *             Error establishing a connection with the DB.
	 */
	private Database() throws ConnectionException {
		Logger.debug("[Database.Database]");
		AstyanaxConfigurationImpl astconfig = new AstyanaxConfigurationImpl()
				.setDiscoveryType(NodeDiscoveryType.RING_DESCRIBE);
		
		ConnectionPoolConfigurationImpl poolconfig = new ConnectionPoolConfigurationImpl(CONNECTIONPOOL_NAME)
				.setPort(DATABASE_POST)
				.setMaxConnsPerHost(1)
				.setSeeds(DATABASE_HOST + ":" + DATABASE_POST);

		AstyanaxContext<Keyspace> context = new AstyanaxContext.Builder()
				.forCluster(CLUSTER_NAME).forKeyspace(KEYSPACE_NAME)
				.withAstyanaxConfiguration(astconfig)
				.withConnectionPoolConfiguration(poolconfig)
				.withConnectionPoolMonitor(new CountingConnectionPoolMonitor())
				.buildKeyspace(ThriftFamilyFactory.getInstance());
		
		context.start();
		
		KEYSPACE = context.getEntity();
		KEYSPACE.describeKeyspace();	// check if something went wrong
	}
	
}
