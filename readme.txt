--------------------------------------------------------
README
--------------------------------------------------------
Author: Valerio Di Gregorio

Sample project developed using:
- Play! Framework v1.2.5 (Java)
- Cassandra v1.1.12 (Datastax Community Edition for Windows)
- Astyanax v1.56.26
- Java v1.7
- jQuery v1.6.4
- D3.js v3
- World Weather Online (WWO) Free API

Additional tools:
- Eclipse IDE + Scala IDE and Play! 2 support + Subclipse
- TortoiseSVN v1.8.5
- Google Chrome 33.0.1750.154 m
- Fiddler2 v4.4.6.2
- Scriptular.com
- WinMerge

Steps:
1. Install Cassandra and import Weather\conf\initial-data.ddl with cqlsh.
2. Add your WWO Free API key to Weather\app\controllers\Weather.java, see Weather.WWO_API_KEY.
3. Run "play dependencies"
4. Run "play run"
5. Open *Chrome* and type the URL (e.g. localhost:9000)
6. Cross fingers

Default users are:
- homer@simpsons.com
- marge@simpsons.com
- lisa@simpsons.com
- bart@simpsons.com
- maggie@simpsons.com
Their password is "password".
