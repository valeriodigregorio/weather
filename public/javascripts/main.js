/**
 * List of the REST services URLs used by this page.
 */
var Url = {
	WEATHER : "http://localhost:9000/weather/data",
}

/**
 * Utility that displays different types of message boxes.
 */
var MessageBox = {
	CLEAR   : {},
	ERROR   : {style:"error",   tag:"Error!"},
	SUCCESS : {style:"success", tag:"Success!"},
	WARNING : {style:"block",   tag:"Warning!"},
	INFO    : {style:"info",    tag:"Info!"},
	

	/**
	 * Utility that displays different types of message boxes.
	 * 
	 * @param type
	 *            Type of the message box. Specify CLEAR if you want to
	 *            remove all the currently displayed message boxes.
	 * @param message
	 *            The content of the message box.
	 */
	show : function(type, message) {
		$('#notifications').empty();
		if(type === this.CLEAR) return;
		if(!message) message = "";
		message = message.replace("\n","<br>");
		var element = '<div class="alert alert-' + type.style + '"><p><strong>' + type.tag + '</strong> ' + message + '</p></div>';
		$(element).appendTo('#notifications');
	}
}
