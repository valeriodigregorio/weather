/**
 * Callback for jQuery.ajax. Parse an XHR error and display a message box.
 * 
 * @param xhr
 *            The jqXHR object.
 * @param status
 *            A string describing the type of error that occurred.
 * @param error
 *            An optional exception object, e.g., HTTP error code textual
 *            description.
 */
function onError(xhr, status, error) {
	console.debug("[weather.onError] xhr = ", xhr, "; status = ", status, "; error = ", error);
	
	var type = MessageBox.ERROR;
	if(xhr.status >= 200 && xhr.status <= 300)
		type = MessageBox.SUCCESS;
	
	if (xhr.status === 0) {
		message = "Not connected, please verify your network connection.";
	} else {
		if(xhr.status === 429) error = "Too Many Requests."; // see RFC 6585
		message = error + " (HTTP " + xhr.status + ")";
		// I'd like to parse xhr.responseText too but the JSON returned by play
		// is malformed, see http://bit.ly/QUpJKj
	}
	
    MessageBox.show(type, message);
}

/**
 * Callback for Url.WEATHER result. It parses the response and displays the data
 * in the UI.
 * 
 * @param response
 *            The JSON response of call to Url.WEATHER.
 */
function onSuccess(response) {
	console.debug("[weather.onSuccess] response = ", response);
	
	MessageBox.show(MessageBox.CLEAR);
	
	try {
		var location = response.data.nearest_area[0];
		var request = response.data.request[0];
		var condition = response.data.weather[0];
		
		$('#ipaddress').val(request.query);
		$('#condition').empty();
		$('#info').empty();
		
		// Icon
		var element = '<img id="condition" class="icon" src="' + condition.weatherIconUrl[0].value + '" alt="' + condition.weatherDesc[0].value + '" />';
		$(element).appendTo('#condition');
		
		// Description
		element = '<div id="description" class="text">' + condition.weatherDesc[0].value + '</span></p>';
		$(element).appendTo('#condition');
		
		// City and region
		element = '<div id="location" class="text">' + location.areaName[0].value + ', ' + location.region[0].value + '</div>';
		$(element).appendTo('#info');
		
		// Country
		element = '<div id="location" class="text">' + location.country[0].value + '</div>';
		$(element).appendTo('#info');
		
		// Date
		element = '<div id="date">' + response.data.weather[0].date + '</span></div>';
		$(element).appendTo('#info');
		
		// Temperature (Celsius)
		element = '<div id="temperature"><span class="value">' + condition.tempMinC + '</span><span class="text">&deg;C</span> / <span class="value">' + condition.tempMaxC + '</span><span class="text">&deg;C</span></div>';
		$(element).appendTo('#info');
		
	    drawBars(response.data.weather);
	} catch(e) {
		try {
			MessageBox.show(MessageBox.ERROR, response.data.error[0].msg);
		} catch(ee) {
			MessageBox.show(MessageBox.ERROR, "Invalid data.");	
		}
	}
}

/**
 * Trigger an async refresh of the UI.
 * 
 * @param ipaddress
 *            The IP address to use for the refresh. Null or empty string mean
 *            local IP address.
 */
function doRefresh(ipaddress) {
	console.debug("[weather.doRefresh] ipaddress = ", ipaddress);
	
	// actually this validation is not necessary because removing it the
	// user can query the server using city names, countries, etc. they
	// works exactly like IPs.
	if(ipaddress) {
		var regexp = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";
		if(!ipaddress.match(regexp)) {
			MessageBox.show(MessageBox.INFO, "A valid IPv4 address has the format X.X.X.X where 0 >= X >= 255.");
			return false;
		}
	}
	
	var request = {
		type: "GET",
		url: Url.WEATHER,
		data : {},
		dataType: "jsonp",
		success: onSuccess,
		error: onError,
	}
	
	if(ipaddress && ipaddress.length > 0)
		request.data.ipaddress = ipaddress;
	
	$.ajax(request);
	
	// stop form submission
	return false;
}

function drawBars(array) {
	console.debug("[weather.drawBars] array = ", array);
	$("#d3bars").empty();
	var margin = {top: 30, right: 10, bottom: 10, left: 10},
    width = $("#d3bars").width() - margin.left - margin.right,
    height = $("#d3bars").height() - margin.top - margin.bottom;

	var x = d3.scale.linear()
	    .range([0, width]);
	
	var y = d3.scale.ordinal()
	    .rangeRoundBands([0, height], .2);
	;
	var xAxis = d3.svg.axis()
	    .scale(x)
	    .orient("top");
	
	var svg = d3.select("#d3bars").append("svg")
	    .attr("width", width + margin.left + margin.right)
	    .attr("height", height + margin.top + margin.bottom)
	    .append("g")
	    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
	
	// data
	var data = [];
	array.forEach(function(entry) {
	    data.push({"date": entry.date, "min": entry.tempMinC, "max":  entry.tempMaxC});
	});
	
	console.debug("[weather.drawBars] data = ", data);
	
	// domains
	var tmin = d3.min(data, function(d) { return +d.min; });
	var tmax = d3.max(data, function(d) { return +d.max; });
		
	x.domain([tmin, tmax]);
	y.domain(data.map(function(d) { return d.date; }));
	
	var tcolor = Math.min(Math.abs(tmin), Math.abs(tmax));
	
	// axis
	svg.append("g")
		.attr("class", "x axis")
		.call(xAxis);
	
	// bars gradient
	var gradient = svg
		.append("linearGradient")
		.attr("y1", "0")
		.attr("y2", "0")
		.attr("x1", x(0))	// this is cold
		.attr("x2", x(18))	// this is almost warm... ok I'm Italian! 
		.attr("id", "gradient")
		.attr("gradientUnits", "userSpaceOnUse")

    gradient.append("svg:stop")
		.attr("offset", "0%")
		.attr("stop-color", "steelblue")
		.attr("stop-opacity", 1);

    gradient.append("svg:stop")
		.attr("offset", "100%")
		.attr("stop-color", "brown")
		.attr("stop-opacity", 1);

    // bars values
	svg.selectAll(".bar")
		.data(data)
		.enter().append("rect")
		.attr("class", "bar")
		.attr("x", function(d) { return x(d.min); })
		.attr("y", function(d) { return y(d.date); })
		.attr("width", function(d) { return x(d.max) - x(d.min); })
		.attr("height", y.rangeBand())
		.style("fill", "url(#gradient)");
	
	// labels
	svg.selectAll(".label")
		.data(data)
		.enter().append("text")
		.attr("class", "label")
		.attr("x", function(d) { return (x(d.min) + x(d.max)) / 2; })
		.attr("y", function(d) { return y(d.date) + y.rangeBand() / 2; })
		.attr("dy", ".40em")
		.text(function(d) { return d.date; });
}

$(document).ready(function() {
	console.debug("[weather.ready]");
	
	// Rotating effect for refresh button, so that the user gets feedback
	// from the UI.
	var angle = 0;
	$("#refresh").rotate({ 
		bind: {
		   click: function() {
			   angle -= 360;
			   $(this).rotate({animateTo: angle})
		   }
		} 
	});
	
    // Refresh at startup
	doRefresh();
});
