CREATE KEYSPACE weather
WITH strategy_class = 'org.apache.cassandra.locator.SimpleStrategy'
AND strategy_options:replication_factor='1';

USE weather;

CREATE TABLE users (
email varchar PRIMARY KEY,
password varchar,
fullname varchar
);

CREATE TABLE data (
ipaddress varchar PRIMARY KEY,
weather varchar
);

INSERT INTO users (email, password, fullname) VALUES
('homer@simpsons.com', 'password', 'Homer J. Simpsons');

INSERT INTO users (email, password, fullname) VALUES
('marge@simpsons.com', 'password', 'Marjorie Bouvier');

INSERT INTO users (email, password, fullname) VALUES
('lisa@simpsons.com', 'password', 'Elisabeth M. Simpson');

INSERT INTO users (email, password, fullname) VALUES
('bart@simpsons.com', 'password', 'Bartholomew J. Simpson');

INSERT INTO users (email, password, fullname) VALUES
('maggie@simpsons.com', 'password', 'Margaret E. Simpson');
